package main

import "fmt"

func main() {
	//int age=30; c++ way
	var age int = 30
	fmt.Println("Age:", age)

	var name = "Kevin"
	//fmt.Println("Your name is:", name)

	//used to mute the compile time error
	_ = name

	//this is the short declaration operator

	s := "Learning golang!"

	fmt.Println(s)

	//multiple variable declaration
	car, cost := "Audi", 50000
	fmt.Println(car, cost)

	//in reassigning, atleat one variable shoul be new

	car, year := "BMW", 2023
	fmt.Println(car, year)

	var opened = false
	opened, file := true, "k.txt"
	fmt.Println(opened, file)

	//For easy readerbility
	var (
		salary    float64
		firstName string
		gender    bool
	)
	_, _, _ = salary, firstName, gender

	var a, b, c int
	fmt.Println(a, b, c)

	//multiple assignments

	var i, j int
	i, j = 5, 8
	// _,_ = i,j
	j, i = i, j //swapping vars
	fmt.Println(i, j)

	sum := 5 + 2.3
	fmt.Println(sum)

}
