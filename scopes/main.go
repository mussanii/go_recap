package main

import (
	"fmt"
	f "fmt" //permitted in Go
)

const done = false //package scoped

func main() {

	var task = "Running" //local(block) scoped
	fmt.Println(task, done)
	f.Println("Bye Bye!")

	const done = true //local(block) scoped
	f.Printf("done in main() is %v\n", done)
	f1()

}

func f1() {
	fmt.Printf("done in f1(): %v\n", done)
}
