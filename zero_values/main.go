package main

import "fmt"

func main() {
	var a = 4
	var b = 5.2

	a = int(b)

	fmt.Println(a, b)

	//You cannnot typecast an already declared variable

	// var x int
	// x = "5"
	// fmt.Println(x)
	var (
		value int
		price float64
		name  string
		done  bool
	)
	fmt.Println(value, price, name, done)
}
