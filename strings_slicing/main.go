package main

import "fmt"

func main() {
	s1 := "i love Golang!"
	fmt.Println(s1[2:5])
	s2 := "绘制爱心文本 - 王诗翔"
	rs := []rune(s2)
	fmt.Printf("%T\n", rs)
	fmt.Println(string(rs[0:3]))

}
