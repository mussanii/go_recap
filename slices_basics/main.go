package main

import "fmt"

func main() {
	var cities []string
	fmt.Println("Cities is equal to nil: ", cities == nil)
	fmt.Printf("Cities %#v\n", cities)
	//You cannot assign a value to a nil slice
	//eg cities[0] = "Nairobi"
	numbers := []int{1, 2, 3, 4, 5}
	fmt.Println(numbers)

	nums := make([]int, 2)
	fmt.Printf("%#v\n", nums)

	type names []string
	friends := names{"John", "Kevin", "Omosh"}
	fmt.Println(friends)

	myFriend := friends[0]
	fmt.Println("My best friend is ", myFriend)
	//we can modify elements in a slice using indexes
	friends[1] = "Anne"
	fmt.Println(friends)

	for index, value := range numbers {
		fmt.Printf("index: %v, value: %v\n", index, value)
	}

}
