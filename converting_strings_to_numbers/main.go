package main

import (
	"fmt"
	"strconv"
)

func main() {
	s := string(99)
	fmt.Println(s)

	var myStr = fmt.Sprintf("%f", 44.5)
	fmt.Println(myStr)

	var myStr1 = fmt.Sprintf("%d", 65656)
	fmt.Println(myStr1)

	s1 := "3.123" //typpe string
	fmt.Printf("%T\n", s1)
	var f1, err = strconv.ParseFloat(s1, 64)
	_ = err
	fmt.Println(f1)
	i, err := strconv.Atoi("-50")
	s2 := strconv.Itoa(20)
	fmt.Printf(" i is type %T, i value is %v\n", i, i)
	fmt.Printf("  s2 type %T, s2 value is %v\n", s2, s2)

}
