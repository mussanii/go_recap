package main

import "fmt"

func main() {
	a := [5]int{1, 2, 3, 4, 5}
	//a[start:stop]
	b := a[1:3] //exludes the 3rd index
	//_ = b
	fmt.Printf("%v, %T", b, b)

	s1 := []int{1, 2, 3, 4, 5, 6}

	s2 := s1[1:3]
	fmt.Println(s2)
	//this will print from index 2 to the end of the slice
	fmt.Println(s1[2:])
	fmt.Println(s1[:3]) //prints from index zero  to 2 exluding index 3
	fmt.Println(s1[:])  //this will print everything

	s1 = append(s1[:4], 300) //overwrites element at stated index
	fmt.Println(s1)

}
