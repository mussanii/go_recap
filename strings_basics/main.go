package main

import "fmt"

func main() {
	s1 := "\"Hi there Go!\""
	fmt.Println(s1)
	fmt.Println(`He says: "Hello!"`)
	//raw string
	s2 := `I like \n Go`
	fmt.Println(s2)

}
