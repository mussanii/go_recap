package main

import "fmt"

func main() {
	s1 := []int{10, 20, 30, 40, 50}
	s3, s4 := s1[0:2], s1[1:3]
	s3[1] = 600
	fmt.Println(s1, s4)

	arr1 := [4]int{5, 10, 15, 20}
	slice1, slice2 := arr1[0:2], arr1[1:3]
	slice1[1] = 25

	fmt.Println(arr1)
	fmt.Println(slice2)

	//creating new slice from the backing array  even after modifying slice index 0

	cars := []string{"Ford", "Audi", "Toyota", "Range Rover"}
	newCars := []string{}
	//append first
	newCars = append(newCars, cars[0:2]...)
	//modify later
	cars[0] = "John Dere"
	fmt.Println(cars, newCars)

	ss1 := []int{10, 20, 30, 40, 50}
	newSlice := ss1[0:3]
	fmt.Println(len(newSlice), cap(newSlice))

	newSlice = ss1[2:5]
	fmt.Println(len(newSlice), cap(newSlice))

	fmt.Printf("%p\n", &ss1)
	fmt.Printf("%p %p \n", &ss1, &newSlice)

}
