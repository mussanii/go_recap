package main

import (
	"fmt"
)

func main() {

	//typed const
	const a float64 = 5.1

	//untyped const
	const b = 6.7

	const c float64 = a * b

	const str = "Hello" + "go"

	const d = 5 > 10

	fmt.Println(d)

	//strongly types
	// const x int = 5
	// const y float64 = 2.2 * x

	const x = 5
	const y = 2.2 * x

	fmt.Println(y)

	var i int = x     //x changes to int
	var j float64 = x // var j float64 = float64(x)
	var p byte = x    //x changes to byte
	fmt.Println(i, j, p)

}
