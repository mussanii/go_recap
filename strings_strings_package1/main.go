package main

import (
	"fmt"
	"strings"
)

func main() {
	p := fmt.Println
	result := strings.Contains("I love Go Programming", "lovex")
	p(result)

	result = strings.ContainsAny("success", "xy")
	p(result)

	result = strings.ContainsRune("golang", 'g')
	p(result)
	n := strings.Count("cheese", "e")
	p(n)

	n = strings.Count("Five", "")
	p(n)
	p(strings.ToLower("KEVIN GO PYTHON"))
	p(strings.ToUpper("KEVIN GO pyTHON"))
	p("go" == "go")

	myStr := strings.Replace("192.1698.1.0", ".", ":", 2)
	p(myStr)
	myStr = strings.Replace("192.1698.1.0", ".", ":", -1)
	p(myStr)

	myStr = strings.ReplaceAll("192.1698.1.0", ".", ":")
	p(myStr)

}
