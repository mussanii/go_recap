package main

import "fmt"

func main() {
	const days int = 7

	var i int
	fmt.Println(i)

	const pi float64 = 3.14
	const secondsInHour = 3600

	duration := 234 //in hrs
	fmt.Printf("Duration in seconds: %v\n", duration*secondsInHour)

	//runtime error
	// x, y := 5, 0
	// fmt.Println(x / y)

	//compile time error

	// const a = 5
	// const b = 0
	// fmt.Println(a / b)

	// //declaring multiple constsconst
	// const n, m int = 4, 5
	// //You can multi-declare const without types
	// const n1, n2 = 8, 7

	//OR
	const (
		min1 = -200
		min2 = -400
		min3 = 100
	)
	fmt.Println(min1, min2, min3)

	//constants rules
	//1. You cannot change a constant
	//2.You cannot init a constant at runtime
	//eg const POweer = Math.Pow(2,3)
	//3. You cannot use a variable to initialize a const
	//eg
	//  t :=5
	// const tc = t

	//4. We can init a const using the len func cz its a builtin function
	//eg cont l1 = len("hello")
}
