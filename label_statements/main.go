package main

import "fmt"

func main() {
	people := [5]string{"James", "Jannet", "John", "June", "Duke"}
	friends := [3]string{"June", "Dee", "Duke"}

outer:
	for index, name := range people {

		for _, friend := range friends {
			if name == friend {
				fmt.Printf("Found a friend %q at index %d\n", friend, index)
				break outer
			}
		}
	}
	fmt.Println("Proceed with the rest of instructions after the break!")

}
