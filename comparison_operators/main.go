package main

import "fmt"

func main() {
	//comparison operators
	a, b := 5, 10
	fmt.Println(a == b)
	fmt.Println(a != b)
	fmt.Println(a > 5, a >= 5)
	fmt.Println(b < a, 10 <= b)

	//Logical operators
	fmt.Println(a < 1 && b == 10)
	fmt.Println(a == 5 || b == 100)
	fmt.Println(!(a > 0))

}
