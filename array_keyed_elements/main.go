package main

import "fmt"

func main() {
	grades := [3]int{
		1: 3,
		0: 50,
		2: 90,
	}
	fmt.Println(grades)

	accounts := [3]int{
		2: 50,
	}
	fmt.Println(accounts)

	names := [...]string{6: "Kevin"}

	fmt.Println(names)

	weekend := [7]bool{5: true, 6: true}
	fmt.Println(weekend)

}
