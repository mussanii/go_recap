package main

import "fmt"

func main() {
	a, b := 4, 2
	r := (a + b) / (a - b) * 2
	fmt.Println(r)

	//modulus operatir
	r = 9 % a
	fmt.Println(r)

	//assignment opreators
	x, y := 2, 3

	//increment
	x += y
	fmt.Println(x)

	//decrement
	y -= 2
	fmt.Println(y)

	//multiplication
	y *= 10
	fmt.Println(y)

	//division
	y /= 5
	fmt.Println(y)

	//modulus
	x %= 3
	fmt.Println(x)

	s := 1
	s += 1
	s++
	fmt.Println(s)
	s--
	fmt.Println(s)

}
