package main

import "fmt"

func main() {
	var nums []int
	//type and contents
	fmt.Printf("%#v\n", nums)
	//length and capacity
	fmt.Printf("Length: %d, Capacity %d \n", len(nums), cap(nums))
	nums = append(nums, 1, 2)
	fmt.Printf("Length: %d, Capacity %d \n", len(nums), cap(nums))
	//this will create a larger backing array so that it wont nead to create a new one if you keep on appending
	nums = append(nums, 3)
	fmt.Printf("Length: %d, Capacity %d \n", len(nums), cap(nums)) //Length: 3, Capacity 4

	nums = append(nums, 4)
	fmt.Printf("Length: %d, Capacity %d \n", len(nums), cap(nums)) //Length: 4, Capacity 4

	nums = append(nums, 5)
	fmt.Printf("Length: %d, Capacity %d \n", len(nums), cap(nums)) //Length: 5, Capacity 8

	nums = append(nums[0:5], 230, 400, 590, 600)
	fmt.Printf("Length: %d, Capacity %d \n", len(nums), cap(nums)) //Length: 9, Capacity 16

	letters := []string{"A", "B", "C", "D", "E", "F"}
	letters = append(letters[:1], "X", "Y")
	fmt.Printf("Length: %d, Capacity %d \n", len(letters), cap(letters)) //Length: 9, Capacity 16
	fmt.Println(letters)

}
