package main

import "fmt"

func main() {
	var n []int
	fmt.Println(n == nil)

	m := []int{}
	fmt.Println(m == nil)

	//we cannot compare slices using the equal operator, it can only be compared to nil
	a, b := []int{1, 2, 3}, []int{1, 2, 3}
	//fmt.Println(a == b) // invalid can only be compared to nil

	//to compare, do this

	var eq bool = true

	//1) A problem introduction
	a = nil
	for i, valueA := range a {
		if valueA != b[i] {
			eq = false
			break
		}
	}

	// 2) Problem solution
	if len(a) != len(b) {
		eq = false
	}

	if eq {
		fmt.Println("a and b slices are equal")
	} else {
		fmt.Println("a and b slices are not equal")
	}

}
