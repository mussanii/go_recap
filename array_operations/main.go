package main

import (
	"fmt"
	"strings"
)

func main() {
	numbers := [3]int{10, 20, 30}
	fmt.Printf("%#v\n", numbers)

	//modifying elements
	numbers[0] = 23
	fmt.Printf("%#v\n", numbers)

	//array iteration

	for i, v := range numbers {
		fmt.Println("index:", i, " value:", v)

	}

	for i := 0; i < len(numbers); i++ {
		fmt.Println("index:", i, "value", numbers[i])

	}
	fmt.Println(strings.Repeat("%", 50))

	//dimentional arrays
	balances := [2][3]int{
		{3, 5, 8},
		[3]int{4, 9, 6},
	}
	fmt.Println(balances)
	fmt.Println(strings.Repeat("%", 50))
	m := [3]int{3, 5, 6}
	n := m
	fmt.Println("n is equal to m:", n == m)

}
