package main

import "fmt"

func main() {
	var numbers [4]int
	fmt.Printf("%v\n", numbers)
	fmt.Printf("%#v\n", numbers)

	//ways of declaring arrays
	var a1 = [4]float64{}

	fmt.Printf("%#v\n", a1)
	//declaration with elements
	var a2 = [3]int{-10, 15, 3}
	fmt.Printf("%#v\n", a2)
	//short form declaration
	a3 := [3]string{"kev", "john", "Dan"}
	fmt.Printf("%#v\n", a3)
	//not amust to init all elements
	a4 := [3]string{"aa", "bb"}
	fmt.Printf("%#v\n", a4)

	//ellipsis operator. you can write as many elements as you want and iit will detect
	a5 := [...]int{1, 22, 33, 44, 66, 798, 90, 67}
	fmt.Printf("%#v\n", a5)

	//when writting elements in different lines they should have a comma each

	a6 := [...]int{1,
		2,
		3,
		4,
		5, //this coma is mandatory

	}
	fmt.Printf("%#v\n", a6)

}
