package main

import (
	"fmt"
	"os"
	"strconv"
)

func main() {
	// fmt.Println("os.args", os.Args)
	// fmt.Println("Path", os.Args[0])
	// fmt.Println("os.args", os.Args[1])
	// fmt.Println("# of items inside os.Args", len(os.Args))
	var result, err = strconv.ParseFloat(os.Args[1], 64)
	//fmt.Println(result)
	_ = err

	fmt.Printf("%T\n", os.Args[1])
	fmt.Printf("%T\n", result)

}
