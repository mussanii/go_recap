package main

import (
	"fmt"
	"time"
)

func main() {
	language := "golang"

	switch language {
	case "Python":
		fmt.Println("You are learning Python! You dont use curly braces but identation")
	case "Go", "golang":
		fmt.Println("Good, go for Go! You are using curly braces {}")
	default:
		fmt.Println("Any other programming language is a good start!")
	}

	hour := time.Now().Hour()

	switch true {
	case hour < 12:
		fmt.Println("Good morning!")

	case hour < 17:
		fmt.Println("Good afternoon!")

	default:
		fmt.Println("Good evening!")

	}
}
