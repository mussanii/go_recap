package main

import "fmt"

func main() {
	numbers := []int{1, 2}
	numbers = append(numbers, 3)

	fmt.Println(numbers)

	//appending an element of slice 1 to slice 2
	n := []int{150, 300}
	numbers = append(numbers, n...)
	fmt.Println(numbers)

	//copying elements of a slice to another slice
	src := []int{20, 30, 45}
	dst := make([]int, 2)
	nn := copy(dst, src)
	fmt.Println(src, dst, nn)

}
